from app import db, mongo
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from . import DB


class User(object):
    id: str

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password_hash = self.__generate_password(password)

    @staticmethod
    def __generate_password(new_password):
        return generate_password_hash(new_password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def insert(self):
        if not DB.find_one("users", {"username": self.username}):
            DB.insert(collection='users', data=self.json())

    @classmethod
    def find(cls, username):
        user = DB.find_one("users", {"username": username})
        if user:
            cls.id = str(user.get("_id"))
            cls.username = user.get("username")
            cls.email = user.get("email")
            cls.password_hash = user.get("password_hash")
            return cls
        else:
            return None

    def json(self):
        return {
            'username': self.username,
            'email': self.email,
            'password_hash': self.password_hash
        }


class Thread(object):
    def __init__(self, body, user_id):
        self.body = body
        self.user_id = user_id
        self.timestamp = datetime.utcnow()

    def insert(self):
        DB.insert(collection='threads', data=self.json())

    def json(self):
        return {
            'body': self.body,
            'user_id': self.user_id,
            'timestamp': self.timestamp,
        }


class Post(object):
    def __init__(self, body, thread_id, user_id):
        self.body = body
        self.thread_id = thread_id
        self.user_id = user_id
        self.timestamp = datetime.utcnow()

    def insert(self):
        DB.insert(collection='posts', data=self.json())

    def json(self):
        return {
            'body': self.body,
            'thread_id': self.thread_id,
            'user_id': self.user_id,
            'timestamp': self.timestamp,
        }
