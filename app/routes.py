from . import app, DB, mongo
from app.models import User, Thread, Post
from flask import jsonify, make_response, request, abort
from flask_jwt import jwt_required, current_identity
from flask_pymongo import ObjectId


@app.route("/")
def index():
    return "Hello world!"


@app.route("/api/v0.1/threads", methods=["GET"])
def list_threads():
    threads = mongo.db.threads.find()
    result = []
    for thread in threads:
        result.append({
            'id': str(thread.get("_id")),
            'body': thread.get("body"),
            'author': str(mongo.db.users.find_one({'_id': ObjectId(thread.get('user_id'))}).get("_id")) if thread.get("user_id") else None
        })
    return jsonify(result)


@app.route("/api/v0.1/threads", methods=["POST"])
@jwt_required()
def new_thread():
    if not request.json or 'body' not in request.json:
        abort(400)
    thread = Thread(body=request.json.get('body'), user_id=current_identity.get("_id"))
    thread.insert()
    return jsonify({
        'status': "You've created a new thread."
    }), 201


@app.route("/api/v0.1/threads/<thread_id>")
def get_thread(thread_id: str):
    thread = mongo.db.threads.find({"_id": ObjectId(thread_id)})
    posts_raw = mongo.db.posts.find({"thread_id": ObjectId(thread_id)})
    posts = []
    for post in posts_raw:
        posts.append({
            'author': str(post.get("user_id")),
            'body': post.get("body"),
            'timestamp': post.get("timestamp"),
        })
    return jsonify({
        'author': str(thread.get("user_id")) if thread.get("user_id") else None,
        'body': thread.get("body"),
        'posts': posts,
        'timestamp': thread.get("timestamp"),
    })


@app.route("/api/v0.1/threads/<thread_id>", methods=['POST'])
@jwt_required()
def new_post(thread_id: int):
    if not request.json or 'body' not in request.json:
        abort(400)
    mongo.db.threads.find({"_id": thread_id})
    post = Post(body=request.json.get('body'), thread_id=ObjectId(thread_id), user_id=ObjectId(current_identity.get("id")))
    post.insert()
    return jsonify({
        'status': f"You've created a new post in thread {thread_id}."
    }), 201


@app.route("/sign_up", methods=['POST'])
def new_user():
    if not request.json or 'username' not in request.json or 'email' not in request.json or 'password' not in request.json:
        abort(400)
    if mongo.db.users.find({"username": request.json.get('username')}).count():
        abort(400)
    if mongo.db.users.find({"email": request.json.get('email')}).count():
        abort(400)
    user = User(username=request.json.get('username'), email=request.json.get('email'), password=request.json.get('password'))
    user.insert()
    return jsonify({
        'status': 'successfully signed up'
    })


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'nothing here!'}), 404)
