from flask import Flask
from app.config import Config
from flask_migrate import Migrate
from flask_jwt import JWT
from flask_pymongo import PyMongo, ObjectId

app = Flask(__name__)
app.config.from_object(Config)
mongo = PyMongo(app, uri=config.Config.MONGO_URI)
db = None
migrate = Migrate(app, db)


class DB(object):

    @staticmethod
    def init():
        client = mongo.cx

    @staticmethod
    def insert(collection, data):
        mongo.db[collection].insert(data)

    @staticmethod
    def find_one(collection, query):
        return mongo.db[collection].find_one(query)


from app import routes, models


def authenticate(username, password):
    user = models.User.find(username=username)
    if user and user.check_password(self=user, password=password):
        return user


def identity(payload):
    user_id = payload['identity']
    return mongo.db.users.find_one({"_id": ObjectId(user_id)})


jwt = JWT(app, authenticate, identity)
